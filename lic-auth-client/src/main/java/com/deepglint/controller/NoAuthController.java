package com.deepglint.controller;

import com.deepglint.common.bo.ResultBean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @desc
 *
 * @author huangchen@deepglint.com
 * 
 * @date 2019/3/14 11:42
 */
@RestController
@RequestMapping(value = "/noauth/api/1.0", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class NoAuthController {

    @RequestMapping(value = "/openRes", method = RequestMethod.GET)
    public ResultBean<?> openRes() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String obj = MessageFormat.format("今天是：{0}", format.format(new Date()));
        return ResultBean.ok(obj);
    }
}
