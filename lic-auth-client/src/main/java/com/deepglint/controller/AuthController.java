package com.deepglint.controller;

import com.deepglint.common.bo.ResultBean;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;

/**
 * @desc
 *
 * @author huangchen@deepglint.com
 * 
 * @date 2019/3/14 11:42
 */
@RestController
@RequestMapping(value = "/auth/api/1.0", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
public class AuthController {

    @RequestMapping(value = "/getUserInfo")
    public ResultBean<?> getUserInfo() {
        String obj = MessageFormat.format("用户:{0}, 密码：{1}", new Object[]{"deepglint", "123456"});
        return ResultBean.ok(obj);
    }
}
