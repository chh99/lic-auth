# LicenseDemo

#### 项目介绍
在基于Spring的项目中使用 `TrueLicense `生成和验证`License证书`（服务器许可）的示例代码

#### 技术依赖：
* `Spring Boot`：项目基础架构
* `TrueLicense `：基于`Java`实现的生成和验证服务器许可的简单框架

#### 环境依赖：
* `JDK8+`

#### 利用jdk keytool工具制作证书：
* `利用jdk keytool工具生成证书`
    keytool -genkeypair -keysize 1024 -validity 3650 -alias "秘钥别名" -keystore "秘钥库名称.keystore" -storepass "秘钥库口令" -keypass "秘钥口令" -dname "CN=localhost, OU=localhost, O=localhost, L=SH, ST=SH, C=CN"
    注意：所有口令必须包含字母数字
    eg: 
        keytool -genkeypair -keysize 1024 -validity 3650 -alias "privateKey" -keystore "privateKeys.keystore" -storepass "deepglint_store_pwd123" -keypass "deepglint_key_pwd123" -dname "CN=localhost, OU=localhost, O=localhost, L=SH, ST=SH, C=CN"
* `利用jdk keytool工具导出证书文件`
    keytool -exportcert -alias "秘钥别名" -keystore "秘钥库名称.keystore" -storepass "秘钥库口令" -file "证书名称.cer"
    eg: 
        keytool -exportcert -alias "privateKey" -keystore "privateKeys.keystore" -storepass "deepglint_store_pwd123" -file "certfile.cer"
* `利用jdk keytool工具将证书文件导入到证书库中`
    keytool -import -alias "别名" -file "证书名称.cer" -keystore "秘钥库名称.keystore" -storepass "秘钥库口令"
    eg：
        keytool -import -alias "publicCert" -file "certfile.cer" -keystore "publicCerts.keystore" -storepass "deepglint_store_pwd123"
#### 两个子项目说明： ####

- `lic-auth-server`：用于**开发者**给客户生成`License证书`的示例代码
- `lic-auth-client`：**模拟需要给客户部署的业务项目**

#### lic-auth-server项目： ####
对外发布了两个RESTful接口：
（1）获取服务器硬件信息 ：
请求地址：`http://127.0.0.1:10000/license/getServerInfos`
返回：
    {
        "msg": "success",
        "code": 1,
        "data": {
            "ipAddress": [
                "2001:0:2841:aa90:34fb:8e63:c5ce:e345",
                "192.168.153.155"                 #客户机ip
            ],
            "macAddress": [
                "00-00-00-00-00-00-00-E0",
                "B0-52-16-27-F5-EF"               #客户机mac地址
            ],
            "cpuSerial": "178BFBFF00660F51",      #客户机cpu序列号
            "mainBoardSerial": "L1HF7B400HZ"      #客户机主板序列号
        },
        "timestamp": 1552532098275
    }

（2）生成证书 ：
请求地址：`http://127.0.0.1:10000/license/generateLicense
请求时需要在Header中添加一个 **Content-Type** ，其值为：**application/json;charset=UTF-8**。请求参数（json格式）如下： 
{
	"subject": "license_sub",            #证书subject
	"privateAlias": "privateKey",         #秘钥别名
	"keyPass": "deepglint_key_pwd123",    #秘钥口令
	"storePass": "deepglint_store_pwd123",   #秘钥库口令
	"licensePath": "D:/dev/code-bak/license/license.lic",   #存放license文件位置
	"privateKeysStorePath": "D:/dev/jdk1.8_64/bin/privateKeys.keystore",    #秘钥库文件文件
	"issuedTime": "2019-03-14 00:00:01",    #license有效期起始时间
	"expiryTime": "2019-03-14 18:01:00",    #license有效期截止时间
	"licenseCheckModel": {
		"ipAddress": ["192.168.153.155"],       #客户机ip
		"macAddress": ["B0-52-16-27-F5-EF"],    #客户机mac地址
		"cpuSerial": "178BFBFF00660F51",        #客户机cpu序列号
		"mainBoardSerial": "L1HF7B400HZ"        #客户机主板序列号
	}
}


#### lic-auth-client项目： ####
项目启动时安装证书，需要license.lic和先前生成的publicCerts.keystore文件，通过`com.deepglint.license.LicenseCheckListener.java`类实现。API访问时校验证书的可用性，通过`com.deepglint.handler.LicenseCheckInterceptor.java`类实现。
（1）访问受保护资源
请求地址：http://127.0.0.1:10001/auth/api/1.0/getUserInfo
说明：Headers加入参数Content-Type：application/json;charset=UTF-8
返回说明：
    如果证书校验失败，返回
    {
                "msg": "您的证书无效，请核查服务器是否取得授权或重新申请证书！",
                "code": 0,
                "data": null,
                "timestamp": 1552550699097
    }
    否则调用API成功
#### 特别说明： ####

