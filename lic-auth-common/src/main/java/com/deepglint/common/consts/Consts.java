package com.deepglint.common.consts;

public interface Consts {

    //默认编码
    String DEFAULT_ENCODING = "UTF-8";
    //json响应格式
    String CONTENT_TYPE_APP_JSON = "application/json; charset=utf-8";
}
