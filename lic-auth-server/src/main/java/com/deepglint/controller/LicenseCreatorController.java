package com.deepglint.controller;

import com.deepglint.common.bo.ResultBean;
import com.deepglint.common.license.LicenseCreator;
import com.deepglint.common.license.LicenseCreatorParam;
import com.deepglint.common.license.bo.LicenseCheckModel;
import com.deepglint.common.utils.os.AbstractServerInfo;
import com.deepglint.common.utils.os.LinuxServerInfo;
import com.deepglint.common.utils.os.WindowsServerInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc 用于生成证书文件，不能放在给客户部署的代码里
 *
 * @author huangchen@deepglint.com
 * 
 * @date 2019/3/14 10:33
 */
@RestController
@RequestMapping("/license")
public class LicenseCreatorController {

    /**
     * 证书生成路径
     */
    @Value("${license.licensePath}")
    private String licensePath;

    /**
     * 获取服务器硬件信息
     * @param osName 操作系统类型，如果为空则自动判断
     */
    @RequestMapping(value = "/getServerInfos",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResultBean<?> getServerInfos(@RequestParam(value = "osName",required = false) String osName) {
        //操作系统类型
        if(StringUtils.isBlank(osName)){
            osName = System.getProperty("os.name");
        }
        osName = osName.toLowerCase();
        AbstractServerInfo abstractServerInfo;
        //根据不同操作系统类型选择不同的数据获取方法
        if (osName.startsWith("windows")) {
            abstractServerInfo = new WindowsServerInfo();
        } else if (osName.startsWith("linux")) {
            abstractServerInfo = new LinuxServerInfo();
        }else{//其他服务器类型
            abstractServerInfo = new LinuxServerInfo();
        }
        return ResultBean.ok(LicenseCheckModel.installServerInfo(abstractServerInfo));
    }

    /**
     * 生成授权文件
     * @param param 生成授权需要的参数
     * {
     * 	"subject": "license_sub",
     * 	"privateAlias": "privateKey",
     * 	"keyPass": "deepglint_key_pwd123",
     * 	"storePass": "deepglint_store_pwd123",
     * 	"licensePath": "D:/dev/code-bak/license/license.lic",
     * 	"privateKeysStorePath": "D:/dev/jdk1.8_64/bin/privateKeys.keystore",
     * 	"issuedTime": "2019-03-13 00:00:01",
     * 	"expiryTime": "2019-03-16 15:30:00",
     * 	"licenseCheckModel": {
     * 		"ipAddress": ["2001:0:2841:aa90:34fb:8e63:c5ce:e345", "192.168.153.155"],
     * 		"macAddress": ["00-00-00-00-00-00-00-E0","B0-52-16-27-F5-EF"],
     * 		"cpuSerial": "178BFBFF00660F51",
     * 		"mainBoardSerial": "L1HF7B400HZ"
     *        }
     * }
     *
     */
    @RequestMapping(value = "/generateLicense",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    public ResultBean<?> generateLicense(@RequestBody LicenseCreatorParam param) {
        if (StringUtils.isBlank(param.getLicensePath())) {
            param.setLicensePath(licensePath);
        }
        LicenseCreator licenseCreator = new LicenseCreator(param);
        boolean result = licenseCreator.generateLicense();
        if (result) {
            return ResultBean.ok(param, "授权文件生成成功！");
        } else {
            return ResultBean.fail("授权文件生成失败！");
        }
    }
}